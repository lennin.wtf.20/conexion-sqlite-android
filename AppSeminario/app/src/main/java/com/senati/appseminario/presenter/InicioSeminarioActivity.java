package com.senati.appseminario.presenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.senati.appseminario.R;

import java.util.Date;

public class InicioSeminarioActivity extends AppCompatActivity {

    //es para el log
    public static final String TAG="InicioSeminarioActivity";


    EditText edtIdAsis,edtnombreAsis,edtapellidosAsis,edtFecNacAsis,edtdni,edtsexo,edtemailAsis,edttelefonoAsis;
    Button btnNuevo,btnActualizar,btnEliminar;

    //es para inizializar cada uno de los objetos
    public void initObjects(){
        edtIdAsis = findViewById(R.id.edtIdAsis);
        edtnombreAsis = findViewById(R.id.edtnombreAsis);
        edtapellidosAsis = findViewById(R.id.edtnombreAsis);
        edtFecNacAsis = findViewById(R.id.edtfecNacAsis);
        edtdni = findViewById(R.id.edtdni);
        edtsexo = findViewById(R.id.edtsexo);
        edtemailAsis = findViewById(R.id.edtemailAsis);
        edttelefonoAsis = findViewById(R.id.edttelefonoAsis);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnEliminar = findViewById(R.id.btnEliminar);
        this.initObjects(); //Inicializando objetos.
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_seminario);
    }
    //para el boton nuevo
    public void onClickNuevo(View view){
        Log.d(TAG, ">>>Ingreso a metodo onClickNuevo()");
    }
    //para el boton Actualizar
    public void onClickActualizar(View view){
        Log.d(TAG, ">>>Ingreso a metodo onClickActualizar()");
    }

    //para el boton Eliminar
    public void onClickEliminar(View view){
        Log.d(TAG, ">>>Ingreso a metodo onClickEliminar()");
    }
}
