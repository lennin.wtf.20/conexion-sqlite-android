package com.senati.appseminario.model;

import java.util.Date;

public class Asistente {
    private int idAsis;
    private String nombreAsis;
    private String apellidosAsis;
    private Date fecNacAsis;
    private String dni;
    private String sexo;
    private String emailAsis;
    private String telefonoAsis;

    //Constructor()
    public Asistente(int idAsis) { }

    public Asistente(String nombreAsis, String apellidosAsis, Date fecNacAsis, String dni, String sexo, String emailAsis, String telefonoAsis) {
        this.nombreAsis = nombreAsis;
        this.apellidosAsis = apellidosAsis;
        this.fecNacAsis = fecNacAsis;
        this.dni = dni;
        this.sexo = sexo;
        this.emailAsis = emailAsis;
        this.telefonoAsis = telefonoAsis;
    }
    //Getter and Setter

    public int getIdAsis() {
        return idAsis;
    }

    public void setIdAsis(int idAsis) {
        this.idAsis = idAsis;
    }

    public String getNombreAsis() {
        return nombreAsis;
    }

    public void setNombreAsis(String nombreAsis) {
        this.nombreAsis = nombreAsis;
    }

    public String getApellidosAsis() {
        return apellidosAsis;
    }

    public void setApellidosAsis(String apellidosAsis) {
        this.apellidosAsis = apellidosAsis;
    }

    public Date getFecNacAsis() {
        return fecNacAsis;
    }

    public void setFecNacAsis(Date fecNacAsis) {
        this.fecNacAsis = fecNacAsis;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmailAsis() {
        return emailAsis;
    }

    public void setEmailAsis(String emailAsis) {
        this.emailAsis = emailAsis;
    }

    public String getTelefonoAsis() {
        return telefonoAsis;
    }

    public void setTelefonoAsis(String telefonoAsis) {
        this.telefonoAsis = telefonoAsis;
    }
}
